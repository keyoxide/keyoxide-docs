+++
title = "Introduction"
+++

<p class="warning">La documentation en Français est encore en construction et sera limitée en contenu. <a href="/fr/community">Contactez la communauté</a> si vous souhaitez participer!</p>

Keyoxide est un outil éthique pour créer et vérifier des identités décentralisés en ligne.

Comme une carte d'identité pour les identités dans la vraie vie, Keyoxide sert de carte d'identité pour les identités en ligne pour s'assurer qu'on d'interagit avec les personnes authentiques et non des imposteurs. À l'inverse des cartes d'identités de la vraie vie, Keyoxide fonctionne avec des identités en ligne ou "personas" anonymes. Comme toute personne authentique peut posséder plusieurs personas anonymes, ceci protège votre confidentialité en ligne et dans la vraie vie.

## Sur le projet Keyoxide

Le projet Keyoxide est entièrement libre utilisant principalement les licenses Apache et AGPL.

Le code source se trouve sur [Codeberg.org](https://codeberg.org).

[{{ get_it_on_codeberg() }}](https://codeberg.org/keyoxide)

Le projet est entièrement financé par les dons, notamment à travers [Liberapay](https://liberapay.com/keyoxide).

Beaucoup de travail effectué à été financé par la [fondation NLnet](https://nlnet.nl/) et l'initiative de l'Union Européenne [NGI0](https://www.ngi.eu/), pour lequel le projet est très reconnaissant.

[NLnet > Keyoxide](https://nlnet.nl/project/Keyoxide/)  
[NLnet > Keyoxide Private Key Operations](https://nlnet.nl/project/Keyoxide-PKO/)  
[NLnet > Keyoxide v2](https://nlnet.nl/project/Keyoxide-signatures/)  