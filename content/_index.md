+++
title = "Introduction"
+++

Keyoxide is a privacy-friendly tool to create and verify decentralized online identities.

Just like passports for real life identities, Keyoxide can be used to verify the online identity of people to make sure one is interacting with whom they are supposed to be and not imposters. Unlike real life passports, Keyoxide works with online identities or "personas", meaning these identities can be anonymous and one can have multiple separate personas to protect their privacy, both online and in real life.

Ready to get started?

<div class="quick-links">
    <a href="/getting-started/creating-profile">
        <img src="/pen-tool.svg">
        <span>Create a profile</span>
    </a>
    <a href="/getting-started/verifying-profiles">
        <img src="/search.svg">
        <span>Verifying a profile</span>
    </a>
    <a href="/service-providers">
        <img src="/book-open.svg">
        <span>Available claims/proofs</span>
    </a>
</div>

More information:

<div class="quick-links">
    <a href="/clients">
        <img src="/tool.svg">
        <span>Clients & tools</span>
    </a>
    <a href="/faq">
        <img src="/help-circle.svg">
        <span>Frequently Asked Questions</span>
    </a>
    <a href="/getting-started/something-went-wrong">
        <img src="/meh.svg">
        <span>Something went wrong!</span>
    </a>
</div>

## About the Keyoxide project

The Keyoxide project is fully open source with the repositories licensed to either Apache or AGPL.

Our git forge of choice is [Codeberg.org](https://codeberg.org).

The Keyoxide project is entirely funded by donations from individuals and groups, notably through [OpenCollective](https://opencollective.com/keyoxide).

Much of the work has been funded by the [NLnet foundation](https://nlnet.nl/) and [NGI0](https://www.ngi.eu/) for which the project is very grateful.

[NLnet > Keyoxide](https://nlnet.nl/project/Keyoxide/)  
[NLnet > Keyoxide Private Key Operations](https://nlnet.nl/project/Keyoxide-PKO/)  
[NLnet > Keyoxide v2](https://nlnet.nl/project/Keyoxide-signatures/)  

Everyone can join the [Keyoxide community](/community) and contribute in their own way, or just follow along for updates on the project.

## About the Keyoxide documentation

Static site generated using [zola](https://getzola.org). Flags from [flagpack](https://flagpack.xyz). Icons from [Feather icons](https://feathericons.com).

Would you like to help us out with translations? Have a look at the [source code](https://codeberg.org/keyoxide/keyoxide-docs) or [reach out](/community) to get started!