+++
title = "Create a profile (OpenPGP)"

weight = 1
+++

<p class="warning">Read the guide and found it difficult to create a profile? Perhaps you'd prefer the more approachable <a href="/getting-started/creating-profile">ASP method to create profiles</a>.</p>

## Step 1: Preparation

1. Install `gpg`: Linux systems usually already have it or can easily install it through package managers, [Gpg4win](https://www.gpg4win.org/) for Windows systems, [GPGTools](https://gpgtools.org/) for Mac systems.

## Step 2: Generate a cryptographic key

<p class="info">Already have an OpenPGP key? Skip ahead to step 3!</p>

*The **cryptographic key** will be used as "container" for your online identity, just as a passport holds your real-world identity.*

In a terminal:

1. Run `gpg --full-gen-key --expert`.
2. Select `(10) ECC (sign only)` and then `(1) Curve 25519`.
3. Set the key to expire in `2y` (2 years) and confirm with `y`. Set yourself a reminder to renew your key in one year.
4. Enter your real-or-fake name and a *valid* email address — you will receive a confirmation email, so it really needs to be valid. Confirm with `O` as in Okay.
5. Enter a password to protect your key.
6. Follow the remaining instructions. You now have an OpenPGP key.
7. When done, run `gpg -K`, find the key you have just created and copy the 40-character fingerprint (looks something like `5DC026DDD293736A3B305F42B6558943003E6A70` but with different characters). Copy this fingerprint, you will need it in later steps.

## Step 3: Add an identity to your key

<p class="info">This guide assumes you want to verify a Fediverse (<a href="/service-providers/activitypub/">ActivityPub</a>) account like Mastodon/Pleroma/Pixelfed/etc. Have a look at the <strong>Available claims/proofs</strong> section of the documentation and repeat the same steps to verify other accounts/identities.</p>

### Step 3A: The identity proof

<p class="info">This guide assumes you want to use a <strong>Profile URL</strong> proof. To use a different format, have a look at the <a href="/proof-formats">Identity proof formats</a> page.</p>

*The **identity proof** is a reference to your cryptographic key and is publicly posted on an online account.*

In a browser:

1. Log in to your Fediverse account and click **Edit profile** or a similar button.
2. Add `https://keyoxide.org/FPR` to your **About me** section — replace `FPR` with the fingerprint you have previously copied.
3. Save the changes you have made to your account.

### Step 3B: The identity claim

<p class="info">All identity claims start with <b>proof@ariadne.id=</b>. Never change this part! More information on the <a href="/ariadne-identity">Ariadne Identity</a> page.</p>

*The **identity claim** is a reference to your online account and is stored inside your cryptographic key.*

In a terminal:

1. Run `gpg --edit-key FPR` where you have replaced `FPR` with the fingerprint you have previously copied.
2. Run `notation`.
3. Run `proof@ariadne.id=https://fediverse.server/@username` — replace with the URL to your own profile!
4. Enter your password and confirm.
5. Save and quit by running `save`.

## Step 4: Upload your public key

*Your **public key** (or public part of your cryptographic key) needs to be uploaded to a public keyserver to be accessible on Keyoxide. This is safe: a public key is read-only and can't be edited by others.*

In a terminal:

1. Run `gpg --armor --output public.asc --export FPR` — replace `FPR` with your own key's fingerprint).
2. In your browser, go to [keys.openpgp.org/upload](https://keys.openpgp.org/upload) and upload the `public.asc` you have created in the previous step.
3. You will receive a verification email with a link you will need to click.
4. You are now done! Go to `https://keyoxide.org/FPR` — replace with your fingerprint — to view your profile.

## Step 5: Adding an avatar

### Using Libravatar (recommended)

You can [create an account on Libravatar.org](https://www.libravatar.org/) or [run your own instance](https://wiki.libravatar.org/running_your_own/), both methods are supported by Keyoxide.

1. Log in to your Libravatar account.
2. Add the same email address used in your cryptographic key.
3. Upload a profile image and assign it to your email address.

### Using Gravatar 

You can [create an account on Gravatar.com](https://gravatar.com/).

1. Log in to your Gravatar account.
2. Add the same email address used in your cryptographic key.
3. Upload a profile image and assign it to your email address.

## What's next?

<div class="quick-links">
    <a href="/getting-started/verifying-profiles">
        <img src="/search.svg">
        <span>Verifying a profile</span>
    </a>
    <a href="/clients">
        <img src="/tool.svg">
        <span>Clients & tools</span>
    </a>
</div>
