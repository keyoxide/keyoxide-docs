+++
title = "Verifying profiles"

weight = 2
+++

## Verifying a profile

Verifying a proile is straightforward, especially if it's a so-called ASP profile. What you will need is the profile's ASP identifier. Here's the profile identifier of the Keyoxide dev:

```
aspe:keyoxide.org:TOICV3SYXNJP7E4P5AOK5DHW44
```

Simply go to [keyoxide.org](https://keyoxide.org) and enter that identifier in the search bar. This will redirect you to [keyoxide.org/aspe:keyoxide.org:TOICV3SYXNJP7E4P5AOK5DHW44](https://keyoxide.org/aspe:keyoxide.org:TOICV3SYXNJP7E4P5AOK5DHW44).

You could also install the [Keyoxide app](https://mobile.keyoxide.org/) and enter the identifier there!

## Verifying an OpenPGP profile

### Fingerprint identifier

OpenPGP profiles are typically identified by a cryptographic key's fingerprint. The fingerprint — as the name suggests — uniquely identifies the key. Here's an example of a fingerprint:

```
3637202523e7c1309ab79e99ef2dc5827b445f4b
```

Go to the [keyoxide.org/3637202523e7c1309ab79e99ef2dc5827b445f4b](https://keyoxide.org/3637202523e7c1309ab79e99ef2dc5827b445f4b) page. The website will automatically find the public key for you and start the identity verification process.

### Email address identifier

A different identifier is the email address associated with the OpenPGP key.

Go to the [keyoxide.org/hkp/test@doip.rocks](https://keyoxide.org/hkp/test@doip.rocks) page. The website will automatically find the public key for you and start the identity claims verification process.

An email address identifier can also be used to obtain a public key not via keyservers but the so-called [Web Key Directory](/web-key-directory) protocol, used to fetch public keys directly from private domains.

Go to the [keyoxide.org/test@doip.rocks](https://keyoxide.org/test@doip.rocks) page. The website will automatically find the public key for you and start the identity verification process.

<p class="info">Keyoxide URLs specify how the website should fetch the key: <code>keyoxide.org/hkp/…</code> will use keyservers, <code>keyoxide.org/wkd/…</code> will use the WKD protocol. If no protocol is specified, a keyserver will be used for fingerprints. For email addresses, it will first try the WKD protocol and fall back to the keyserver.</p>
