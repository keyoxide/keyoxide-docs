+++
title = "Create a profile"

weight = 0
+++

The quickest way to get started with Keyoxide is by creating an *Ariadne Signature Profile* or ASP! This guide will help you through the process of creating an ASP profile and proving your online identities.

There is a separate guide if you'd prefer the more technical approach and [create a profile using OpenPGP keys](/getting-started/creating-profile-openpgp).

## Preparation

The website you see on [keyoxide.org](https://keyoxide.org) is only responsible for the **verification** part of the process and won't let you create a profile. To **create** a profile, use the [online Keyoxide ASP tool](https://asp.keyoxide.org) or any other tool listed here.

This guide will use the [online Keyoxide ASP tool](https://asp.keyoxide.org).

## asp.keyoxide.org

Visit [asp.keyoxide.org](https://asp.keyoxide.org). You should see something like this:

![Create a new profile with the ASP tool](/images/asp-1.png)

Click on the **Create a new profile** button. You will be asked for a password. There are no specific requirements for the passwords but be sensible and where possible, use a password manager and randomly generated passwords.

The ASP tool will not store the password or send it anywhere. It will use the password to encrypt your *secret key* and forget the password directly afterwards. The next time you wish to edit your profile, the tool will need your *secret key* **and** the password to decrypt it.

When you confirm your password, congratulations, you have created your own ASP which Keyoxide can understand and verify! However, your profile is currently empty so let's give the profile some information.

## Adding profile information

The **Edit your profile** should now be visible on your screen.

![Editing a profile with the ASP tool](/images/asp-2.png)

Every profile needs to have a name so that people verifying your profile know what profile they are looking at. This name however can be real or anonymous! Always be careful when divulging personal information on the internet.

Optionally, you can also add a small description to your profile. Note that people can — and are encouraged to — have different profiles for different purposes — work, hobbies, etc. Perhaps you could explain the purpose of your profile in the description.

## Adding identity claims

The goal of having a Keyoxide profile is to prove your online accounts so that others can verify this. An identity claim is when you claim you control a certain online account on some website or platform. Let's add one to your profile.

![Adding an identity claim](/images/asp-3.png)

Under **Identity claims**, you can select which website/platform you want to use to prove your account. At the time of writing, there are few websites/platform to choose from but know that there are many more possibilities than displayed! Simply choose **Manual input** and visit the [Keyoxide docs](https://docs.keyoxide.org/) (see the *Available claims/proofs* section) to add any compatible claim to your profile.

Click the **Add** button and follow the instructions. Adding a claim is done in two steps:

- add some information about the *claim* like the username and/or instance domain to your profile;
- add a *proof* to your online account.

To add a *proof* to your online account, you usually need to log in to that account and add some text to your account's biography, submit a post to your timeline, or something similar. The ASP tool and the [Keyoxide docs](https://docs.keyoxide.org/) have the exact instructions for each type of online account.

To know exactly what text to use as *proof*, scroll down to the **Identity proofs** section. The tool will present you with two proofs: the *direct proof* which is just your profile's identifier, and the *hashed proof* which is an obfuscated form of your profile's identifier. The latter is useful when you wish to hide the location of your identity profile.

When using *hashed proofs*, make sure to never use the same proof twice on different websites! Click the button **Generate new proofs** to obtain a new proof. The proofs will always be valid, no matter how many you generate.

## Securing your profile

The next step is rather important: make sure you never lose access to your profile!

ASPs don't work like your typical website, where you can just log in and edit your information. Each ASP is secured by a unique *secret key* and the ASP tool never stores it for security reasons. This means that is up to you to securely store your *secret key* if you ever wish to access your profile again and update the information.

In other words, to access your ASP in the future, you will need both the *secret key* and the *password* with which you protect the secret key. It is slightly more inconvenient than only using a password but a lot more secure!

Scroll down to **Security** to see your (encrypted) *Secret key*. Again, store this safely!

![ASP tool security section](/images/asp-4.png)

## Sharing your profile

Next, you will find your profile's *fingerprint*. This is randomly generated and is used to identify your profile!

To share your profile with someone, send them your **URI** — which should look something like *aspe:keyoxide.org:ABCXYZABCXYZABCXYZ*. To make it even easier for the other person, you can send them straight to your Keyoxide profile page by adding your **URI** to the Keyoxide domain *keyoxide.org* — or, of course, the domain of a different Keyoxide instance if you use that instance.

That Keyoxide URL would look like *https://keyoxide.org/aspe:keyoxide.org:ABCXYZABCXYZABCXYZ*.

Here's a real working example: [https://keyoxide.org/aspe:keyoxide.org:6WJK26YKF6WUVPIZTS2I2BIT64](https://keyoxide.org/aspe:keyoxide.org:6WJK26YKF6WUVPIZTS2I2BIT64)

## Uploading your profile

I mentioned above you can share your URI — or Keyoxide URL — and it should just work! Well, not yet. The ASP tool still needs to upload your profile information to the server so that others may see your profile.

This is simply done by clicking the **Save and upload profile** button and entering your password. Yes, you need to enter your password again, I told you the ASP tool wants to forget your password as soon as possible for security reasons! It is not uncommon to have to enter your password multiple times per session.

## Editing your profile in the future

To edit your profile in the future, visit the ASP tool again and enter your *Secret key* then click **Load profile**. You will be asked for your password. After that, it's business as usual as described above.

## Adding an avatar to your profile

ASP profiles don't support custom avatars yet.

## Conclusion

There you have it, your profile!

If all went well, you now have a Keyoxide profile page. And of course, you have stored your secret key and password somewhere safe, preferably a password manager.

## What's next?

<div class="quick-links">
    <a href="/getting-started/verifying-profiles">
        <img src="/search.svg">
        <span>Verifying a profile</span>
    </a>
    <a href="/clients">
        <img src="/tool.svg">
        <span>Clients & tools</span>
    </a>
</div>
