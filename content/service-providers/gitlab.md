+++
title = "Gitlab"

[extra]
claim_syntax = "https://DOMAIN/USERNAME/gitlab_proof"
claim_variables = ["DOMAIN", "USERNAME"]
service_provider_id = "gitlab"
+++

Log in to [gitlab.com](https://gitlab.com) or any other GitLab instance (your profile must be "public", not "private") and click on **New project**.

Set the project name to **Gitlab proof**.

Set the project slug to **gitlab_proof**.

Set the project description to the proof.

After creating the project, copy the link to the project.