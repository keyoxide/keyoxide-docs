+++
title = "ASPE"

[extra]
claim_syntax = "aspe:DOMAIN:FINGERPRINT"
claim_variables = ["DOMAIN", "FINGERPRINT"]
service_provider_id = "aspe"
+++

Using your preferred ASPE client ([web](/asp-profiles/asp-web/), [CLI](https://codeberg.org/keyoxide/kx-aspe-cli)), add the proof as a new identity claim.