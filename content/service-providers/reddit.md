+++
title = "Reddit"

[extra]
claim_syntax = "https://www.reddit.com/user/USERNAME/comments/POST_ID/POST_TITLE/"
claim_variables = ["USERNAME", "POST_ID", "POST_TITLE"]
service_provider_id = "reddit"
+++

Log in to [reddit.com](https://www.reddit.com) and create a new post containing the proof.

After posting, copy the link to the post.