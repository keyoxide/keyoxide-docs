+++
title = "WriteFreely"

[extra]
claim_syntax = "https://DOMAIN/USERNAME/POST_ID"
claim_variables = ["DOMAIN", "USERNAME", "POST_ID"]
service_provider_id = "writefreely"
+++

Log in to your account, add the proof to a new post and publish it.

After publishing, copy the link to the post.