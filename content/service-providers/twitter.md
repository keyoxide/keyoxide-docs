+++
title = "Twitter"

[extra]
claim_syntax = "https://twitter.com/USERNAME/status/TWEET_ID"
claim_variables = ["USERNAME", "TWEET_ID"]
service_provider_id = "twitter"
+++

<p class="warning">
    Since Twitter messes with the formatting of the tweet's message, only
    hashed proofs will work.
</p>

Log in to [twitter.com](https://twitter.com) and compose a new tweet containing the proof.

After posting, copy the link to the tweet.