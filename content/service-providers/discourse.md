+++
title = "Discourse"

[extra]
claim_syntax = "PROFILE_URL"
claim_variables = ["PROFILE_URL"]
service_provider_id = "discourse"
+++

<p class="warning">Note: Your Discourse account must be at least on <a href="https://meta.discourse.org/docs?topic=90752">trust level 1 (Basic)</a>. If your profile page isn't accessible in private browsing mode, <a href="https://codeberg.org/keyoxide/keyoxide-web/issues/160">the Discourse instance configuration may prevent the claim verification from working.</a></p>

Log in to the Discourse instance's and add the proof to your **About me**.

After posting, copy the link to your profile page, it should end with your **/u/USERNAME**: this will be your **PROFILE_URL**.
