+++
title = "Keybase"

[extra]
claim_syntax = "https://keybase.io/USERNAME"
claim_variables = ["USERNAME"]
service_provider_id = "keybase"
+++

<p class="info">Only OpenPGP profiles can verify Keybase accounts for now.</p>

<p class="warning">Uploading your OpenPGP private key to Keybase is NOT recommended and may pose serious security risks! Consider only uploading your OpenPGP public key.</p>

Log in to [Keybase](https://keybase.io) and upload your OpenPGP public key.