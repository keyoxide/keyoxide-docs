+++
title = "OpenPGP"

[extra]
claim_syntax = "openpgp4fpr:OPENPGP_FINGERPRINT"
claim_variables = ["OPENPGP_FINGERPRINT"]
service_provider_id = "openpgp"
+++

Using the [Adding an identity claim](/openpgp-profiles/gnupg/#Adding_an_identity_claim) guide, add the proof as a new "proof@ariadne.id" notation.