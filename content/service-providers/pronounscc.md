+++
title = "pronouns.cc"

[extra]
claim_syntax = "https://pronouns.cc/@USERNAME"
claim_variables = ["USERNAME"]
service_provider_id = "pronounscc"
+++

Log in to [pronouns.cc](https://pronouns.cc/), and edit your profile.

There are two fields that can hold a proof:

- **Bio** can hold any type of proof, while
- **Links** can hold a Profile URL proof.
