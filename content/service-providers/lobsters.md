+++
title = "Lobste.rs"

[extra]
claim_syntax = "https://lobste.rs/~USERNAME"
claim_variables = ["USERNAME"]
service_provider_id = "lobsters"
+++

Log in to [Lobste.rs](https://lobste.rs) and append the proof to the **About** section.