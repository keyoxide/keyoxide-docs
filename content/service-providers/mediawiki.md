+++
title = "MediaWiki"

[extra]
claim_syntax = "https://DOMAIN/SCRIPT_PATH/index.php?oldid=REVID"
claim_variables = ["DOMAIN", "SCRIPT_PATH", "REVID"]
service_provider_id = "mediawiki"
+++

<p class="info">Both the <code>?revid=</code> and <code>?oldid=</code> syntax work, though MediaWiki recommends using <code>?oldid=</code>. No extra URL parameters are required, but any additional ones won't affect the result.</p>
<p class="info">Registration isn't necessary &ndash; unregistered (IP) editors can also use this.</p>
<p class="info">This uses MediaWiki format version 2, which is only supported on MediaWiki version <code>1.25</code> or higher, which, as of 2025, is supported in over 80% of MediaWiki instances.</p>
<p class="warning">You must use the <code>index.php</code> syntax for the URL for this to work.</p>

Edit any page on the wiki, such as your sandbox.

Add the proof as the content or as the comment of the edit.

Either click on **Permanent link** under **Tools**, or go to **View history** and copy the URL of the desired revision.