+++
title = "Lichess"

[extra]
claim_syntax = "https://lichess.org/@/USERNAME"
claim_variables = ["USERNAME"]
service_provider_id = "lichess"
+++

Log in to [lichess.org](https://lichess.org) and add the proof as a link.