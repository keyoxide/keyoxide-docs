+++
title = "IRC"

[extra]
claim_syntax = "irc://IRC_SERVER/NICK"
claim_variables = ["IRC_SERVER", "NICK"]
service_provider_id = "irc"
+++

Log in to the IRC server with your registered nickname and send the
following message:

```
/msg NickServ SET PROPERTY KEY PROOF
```

Make sure to replace PROOF with your proof.

To check whether successful, send (make sure to replace NICK):

```
/msg NickServ TAXONOMY NICK
```

To add more proofs, send:

```
/msg NickServ SET PROPERTY KEY2 PROOF
```