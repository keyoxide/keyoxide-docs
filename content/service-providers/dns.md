+++
title = "DNS"

[extra]
claim_syntax = "dns:DOMAIN?type=TXT"
claim_variables = ["DOMAIN"]
service_provider_id = "dns"
+++

Add a TXT record to the DNS records of the (sub)domain you want to verify. Set the value of the record to the proof. No specific TTL value is required.