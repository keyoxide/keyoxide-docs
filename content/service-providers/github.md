+++
title = "Github"

[extra]
claim_syntax = "https://gist.github.com/USERNAME/GIST_ID"
claim_variables = ["USERNAME", "GIST_ID"]
service_provider_id = "github"
+++

Log in to [github.com](https://github.com) and click on **New gist**.

Name the file **proof.md** and put the proof into it (make sure to replace FINGERPRINT and USERNAME).

After creating a public gist, copy the link to the gist.