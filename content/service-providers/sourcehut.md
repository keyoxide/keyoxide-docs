+++
title = "SourceHut"

[extra]
claim_syntax = "https://git.sr.ht/~USERNAME/REPO_NAME"
claim_variables = ["USERNAME", "REPO_NAME"]
service_provider_id = "sourcehut"
+++

Log in [sr.ht][sourcehut], navigate to **[git][git sourcehut]**, and click on
**Create new repository**.

Set the repository name to **sourcehut_proof**, **keyoxide_proof** or any other
name of your choosing.

Visibility can be set to **Public** or **Unlisted**.

After creating the project, clone the project locally.

In your local repository, create a new file named `proof.md` with your proof as
the contents. Commit this file and push it to the remote `main` branch of the
git repository.

Copy the link to the git repository. It should look like this:
`https://git.sr.ht/~USERNAME/REPO_NAME`.

If you want to use a different branch, use the following format as proof:
`https://git.sr.ht/~USERNAME/REPO_NAME/tree/BRANCH_NAME`.

[sourcehut]: https://sr.ht/
[git sourcehut]: https://git.sr.ht/

