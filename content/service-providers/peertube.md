+++
title = "Peertube"

[extra]
claim_syntax = "https://DOMAIN/a/USERNAME"
claim_variables = ["DOMAIN", "USERNAME"]
service_provider_id = "peertube"
+++

Log in to your account and click on **My account**.

Add the proof to your **Description** section.

After saving the profile, copy the link to your profile.