+++
title = "OpenCollective"

[extra]
claim_syntax = "https://opencollective.com/USERNAME"
claim_variables = ["USERNAME"]
service_provider_id = "opencollective"
+++

Log in to [OpenCollective](https://opencollective.com) and add a proof to the collective's **About** section at the bottom of the page.