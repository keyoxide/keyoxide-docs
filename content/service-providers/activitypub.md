+++
title = "ActivityPub"

[extra]
claim_syntax = "https://DOMAIN/@USERNAME"
claim_variables = ["DOMAIN", "USERNAME"]
service_provider_id = "activitypub"
+++

<div class="info">
    <p>
        This is the claim/proof to be used for Fediverse/ActivityPub-enabled platforms like Mastodon, Pleroma, Pixelfed, Peertube, WriteFreely, etc.
    </p>
    <p>
        There are platform specific instructions available for:
    </p>
    <ul>
        <li><a href="/service-providers/friendica">Friendica</a></li>
        <li><a href="/service-providers/kbin">kbin</a></li>
        <li><a href="/service-providers/lemmy">Lemmy</a></li>
        <li><a href="/service-providers/mastodon">Mastodon</a></li>
        <li><a href="/service-providers/peertube">Peertube</a></li>
        <li><a href="/service-providers/pixelfed">Pixelfed</a></li>
        <li><a href="/service-providers/pleroma">Pleroma</a></li>
        <li><a href="/service-providers/writefreely">WriteFreely</a></li>
    </ul>
</div>

Log in to your account and click on **Edit profile**.

There are two methods to create a proof:

- Add the proof to your **About me** or **Biography** section.
- Add the proof to a post.

After saving the profile (or submitting the post), copy the link to your profile (or to the post containing the proof).

<p class="info">Please note, the link to your profile may look slightly different: <em>https://DOMAIN/@USERNAME</em> or <em>https://DOMAIN/USERNAME</em> or <em>https://DOMAIN/users/USERNAME</em> or <em>…</em></p>