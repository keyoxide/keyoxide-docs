+++
title = "forem"
aliases = ["/service-providers/devto"]

[extra]
claim_syntax = "https://DOMAIN/USERNAME/POST_TITLE"
claim_variables = ["DOMAIN", "USERNAME", "POST_TITLE"]
service_provider_id = "forem"
+++

Log in to a [Forem](https://www.forem.com) instance (such as [dev.to](https://dev.to)) and create a new post containing the proof.

After posting, copy the link to the post. It should look like `https://DOMAIN/USERNAME/POST_TITLE`.