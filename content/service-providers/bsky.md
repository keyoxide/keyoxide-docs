+++
title = "Bluesky"

[extra]
claim_syntax = "https://bsky.app/profile/USERNAME/post/POST_ID"
claim_variables = ["USERNAME", "POST_ID"]
service_provider_id = "bsky"
+++

Log in to [bsky.app](https://bsky.app). Then, there are two possibilities for validating your proof:

To validate with your profile:

1. Go to your profile, click on **Edit Profile**, add your proof to the description, and click on **Save Changes**.
2. Copy the link to your profile. It should look like `https://bsky.app/profile/USERNAME`.

<p class="warning">If you're adding your proof to your profile, do not copy the link to one of your posts. Otherwise, your claim will fail validation.</p>

To validate with a post:

1. Click on **New Post**, add your proof to the text box, and click on **Post**.
2. Copy the link to your new post. It should look like `https://bsky.app/profile/USERNAME/post/POST_ID`.

<p class="info">ADVANCED: It's possible to replace the username in either URL with your DID.</p>
