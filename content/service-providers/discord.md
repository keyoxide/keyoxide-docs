+++
title = "Discord"

[extra]
claim_syntax = "https://discord.gg/INVITE_ID"
claim_variables = ["INVITE_ID"]
service_provider_id = "discord"
+++

Log in to [discord.com](https://www.discord.com) and do the following:
 1. Create a server containing the proof in the server's name, or create a community server (`Server Settings > Community > Enable Community`) and enter the proof in the description (`Server Settings > Community > Overview > Server Description`).
 2. Disable permissions for other users: `Server Settings > Roles > Default permissions > Clear permissions`. You may also want to delete all text and voice channels.
 3. Create an invite. Click `Edit invite link` and make sure it does not expire and has no max number of uses.
 4. Copy the invite url.
