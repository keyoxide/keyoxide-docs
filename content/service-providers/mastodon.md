+++
title = "Mastodon"

[extra]
claim_syntax = "https://DOMAIN/@USERNAME"
claim_variables = ["DOMAIN", "USERNAME"]
service_provider_id = "mastodon"
+++

Log in to your account and click on **Edit profile**.

There are three methods to create a proof:

- Add a new item under **Profile metadata** with a label of your choosing — **OpenPGP**, **Cryptography** or **Keyoxide** could be a meaningful label. The value should be the proof.
- Add the proof to your **About me** or **Biography** section.
- Add the proof to a post.

After saving the profile (or submitting the post), copy the link to your profile (or to the post containing the proof).