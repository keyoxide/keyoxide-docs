+++
title = "Lemmy"

[extra]
claim_syntax = "https://DOMAIN/u/USERNAME"
claim_variables = ["DOMAIN", "USERNAME"]
service_provider_id = "lemmy"
+++

Log in to your account and click on **Settings**.

There are two methods to create a proof:

- Add the proof to your **Bio** section.
- Add the proof to a post.

After saving the profile (or submitting the post), copy the link to your profile (or to the post containing the proof).