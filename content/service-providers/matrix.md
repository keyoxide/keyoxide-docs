+++
title = "Matrix"

[extra]
claim_syntax = "matrix:u/USER_ID?org.keyoxide.r=ROOM_ID&org.keyoxide.e=EVENT_ID"
claim_variables = ["USER_ID", "ROOM_ID", "EVENT_ID"]
service_provider_id = "matrix"
+++

Log in to your Matrix instance, join the
[#doipver:matrix.org](https://matrix.to/#/#doipver:matrix.org) room and send the proof as a message.

Click on **View Source** for that message, you should now see the value for
`room_id` and `event_id`. The `event_id` will start with the `$` symbol; do not
include this symbol in the claim!

The value for `room_id` should be `dBfQZxCoGVmSTujfiv:matrix.org`. The value
for `event_id` is unique to your message.

If your Matrix client does not support **View Source**, choose **Share** or
**Permalink**. The URL obtained should look like this:

```
https://matrix.to/#/ROOM_ID/EVENT_ID?via=...
```

Again, the `ROOM_ID` will start with a `!` symbol and the `EVENT_ID` with a `$`
symbol; do not include these symbols in the claim!

Your `USER_ID` is your actual Matrix user ID which should look like:

```
USERNAME:DOMAIN
```

The `USER_ID` usually starts with the `@` symbol; do not include this symbol in
the claim!