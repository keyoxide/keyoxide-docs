+++
title = "Gitea"

[extra]
claim_syntax = "https://DOMAIN/USERNAME/REPO_NAME"
claim_variables = ["DOMAIN", "USERNAME", "REPO_NAME"]
service_provider_id = "gitea"
+++

<p class="warning">Your account's User Visibility must be set to Public for identity verification to work.</p>

Log in the Gitea instance and click on **Create new repository**.

Set the repository name to **gitea_proof**, **keyoxide_proof** or any other name of your choosing.

Set the project description to the proof.

After creating the project, copy the link to the project.