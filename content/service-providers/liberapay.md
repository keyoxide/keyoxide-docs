+++
title = "Liberapay"

[extra]
claim_syntax = "https://liberapay.com/USERNAME"
claim_variables = ["USERNAME"]
service_provider_id = "liberapay"
+++

Log in to [liberapay.com](https://liberapay.com), edit your profile and add the proof to one of the **Descriptions**.