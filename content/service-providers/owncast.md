+++
title = "Owncast"

[extra]
claim_syntax = "https://DOMAIN"
claim_variables = ["DOMAIN"]
service_provider_id = "owncast"
+++

On the admin page of your Owncast instance, add a new social link with the platform Keyoxide and a URL pointing to your Keyoxide profile page.