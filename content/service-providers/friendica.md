+++
title = "Friendica"

[extra]
claim_syntax = "https://DOMAIN/display/POST_ID"
claim_variables = ["DOMAIN", "POST_ID"]
service_provider_id = "friendica"
+++

<p class="warning">Note: When using expiring posts on Friendica, be sure to star your proof post and exempt starred posts from expiring in the <em>Expiration settings</em> in order to prevent your proof from disappearing.</p>

Log in to your account, add the proof to a new post and publish it.

After publishing, copy the link to the post.