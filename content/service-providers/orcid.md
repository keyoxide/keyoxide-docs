+++
title = "ORCID"

[extra]
claim_syntax = "https://orcid.org/ORCID"
claim_variables = ["ORCID"]
service_provider_id = "orcid"
+++

Log in to [orcid.org](https://orcid.org). Your biography can hold every type of
proof, but you must be ready to make your biography public. URI and hashed URI
proofs can be placed in keywords. The profile URL proof can be added as a
website/social link.

Make sure to make your proof publicly available by selecting _Everyone_ from the
visibility drop down!