+++
title = "XMPP"
aliases = ["/service-providers/xmpp_omemo/"]

[extra]
claim_syntax = "xmpp:XMPP_ID"
claim_variables = ["XMPP_ID"]
service_provider_id = "xmpp"
+++

Visit the [XMPP Ariadne Proof Utility](https://xmpp-util.keyoxide.org/), log in using your XMPP credentials and follow the instructions to add a new proof.

### Optional: instructions to work with OMEMO

Log in to the [Conversations Android app](https://conversations.im/) (or any other XMPP app that supports sharing an XMPP URI) in using your XMPP credentials. Go to **Manage accounts > Share > Share as XMPP URI**. The resulting URI should look something like:

```
xmpp:XMPP_ID?omemo-sid-123456789=A1B2C3D4E5F6G7H8I9
```

This is what you will use as **claim** in the next section.