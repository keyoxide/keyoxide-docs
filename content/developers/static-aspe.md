+++
title = "Implement ASPE on a static site"

weight = 2
+++

## Static ASPE

You have a static web page and would like to serve your ASP profile from it. Not a problem! The ASPE protocol is flexible enough to accommodate for this.

To accomplish this, you will need to perform three steps, two of which Keyoxide has a tool to help you with.

## Step 1: generate a profile JWS

As described in the [Write an ASPE server](@/developers/write-aspe-server.md) guide and in greater detail in the [ASP specification](https://ariadne.id/related/ariadne-signature-profile-0/#4-2-secret-key-encryption-for-clients), the profile JWS is basically a cryptographically signed JSON object that holds all the profile information. All you need is the profile information, an ES256 or Ed25519 keypair and a JOSE library like [jose (JS)](https://www.npmjs.com/package/jose) and [josekit (Rust)](https://crates.io/crates/josekit).

If you wish to create this manually, please refer to the [Write an ASPE server](@/developers/write-aspe-server.md) guide.

You may also use the selfhostable [Keyoxide ASP web tool](https://asp.keyoxide.org) to create your profile. The tool lets you export your profile directly as a profile JWS so that you may upload it directly to your static web page.

## Step 2: compute the profile's fingerprint

Each ASP profile has a unique cryptographic key. Each ASP profile also can only be queried through the fingerprint of that key. Therefore, we now need to figure out what the key's fingerprint is.

The process of computing a key's fingerprint involves mostly hashing and encoding. It is described in [section 2.2 of the ASP spec](https://ariadne.id/related/ariadne-signature-profile-0/#2-2-asp-fingerprint).

For inspiration, here's a [function written in rust](https://codeberg.org/keyoxide/kx-aspe-cli/src/commit/e68f9c0fda0be8268f55b79c937d7e63770bd039/src/secretkey.rs#L199) that computing a key's fingerprint.

If you have created your ASP profile using [Keyoxide ASP web tool](https://asp.keyoxide.org), you will find the key's fingerprint already computed for you and displayed under the *Security* header.

## Step 3: serve the profile JWS

The last step is simply uploading the profile JWS from Step 1 and serve it at an HTTP endpoint containing the fingerprint obtained from Step 2. The HTTP endpoint is determined as follows:

```
'/.well-known/aspe/id/' fingerprint
```

That's it. Make the profile JWS available at this location and you have a static ASPE server!

## More information

All the information provided above is further elaborated on in the [ASP specification](https://ariadne.id/related/ariadne-signature-profile-0).

Feel free to reach out to the [community](https://docs.keyoxide.org/community) for questions and discussions.