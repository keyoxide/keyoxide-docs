+++
title = "Self-hosting Keyoxide"
aliases = ["/selfhosting", "/self-hosting", "/advanced/self-hosting"]

weight = 0

[extra]
toc = true
+++

Self-hosting is an important aspect to the Keyoxide project. Users need to trust the Keyoxide instance they're using to reliably verify identities. Making Keyoxide itself decentralized means no one needs to trust a central server, or the people that run it. If a friend or family member is hosting a Keyoxide instance, it becomes much easier to trust that instance!

Hosting only the Keyoxide web interface will result in some claim verifications failing as they cannot be performed in the browser, such as the DNS check. This is why you would perhaps want to host a so-called *proxy* instance as well. The instructions are below.

## Using docker

Install docker and run:

```
docker run -d -p 3000:3000 codeberg.org/keyoxide/keyoxide-web:latest
```

To run the proxy, run:

```
docker run -d -p 3001:3000 -e ENABLE_MAIN_MODULE=false codeberg.org/keyoxide/keyoxide-web:latest
```

## Using docker-compose

```
keyoxide:
    image: codeberg.org/keyoxide/keyoxide-web:latest
    restart: always
    ports:
        - 3000:3000
    environment:
        - DOMAIN=localhost:3000
        - ENABLE_PROXY_MODULE=false
        - PROXY_HOSTNAME=localhost:3001
        - ONION_URL=
        - ACTIVITYPUB_PUBLIC_KEY=
        - ENABLE_EXPERIMENTAL_CACHE=true
        - ENABLE_EXPERIMENTAL_RATE_LIMITER=true

keyoxide-proxy:
    image: codeberg.org/keyoxide/keyoxide-web:latest
    restart: always
    ports:
        - 3001:3000
    environment:
        - DOMAIN=localhost:3001
        - ENABLE_MAIN_MODULE=false
        - ACTIVITYPUB_URL=
        - ACTIVITYPUB_PRIVATE_KEY=
        - IRC_NICK=
        - MATRIX_ROOM_ID=
        - MATRIX_INSTANCE=
        - MATRIX_ACCESS_TOKEN=
        - TELEGRAM_TOKEN=
        - XMPP_SERVICE=
        - XMPP_USERNAME=
        - XMPP_PASSWORD=
```

## Using node

```bash
git clone https://codeberg.org/keyoxide/keyoxide-web
cd keyoxide-web
yarn # or npm install
yarn run start # or npm run start
```

## Server configuration

You can configure keyoxide-web and keyoxide-proxy through the following environment variables.

### keyoxide-web

Environment variable      | Type     | Required | Description
:-------------------------|:---------|:---------|:-----------
`DOMAIN`                  | `string` | false    | The domain on which the instance is hosted
`ONION_URL`               | `string` | false    | The onion URL that points to the same instance
`PORT`                    | `int`    | false    | The port of the file server (default: 3000)
`PROXY_HOSTNAME`          | `string` | false    | The hostname of the keyoxide-proxy instance to use
`ACTIVITYPUB_PUBLIC_KEY`  | `string` | false    | The public key used to simulate an ActivityPub account and sign HTTP requests

### keyoxide-proxy

Environment variable      | Type     | Required | Description
:-------------------------|:---------|:---------|:-----------
`DOMAIN`                  | `string` | false    | The domain on which the instance is hosted
`ACTIVITYPUB_URL`         | `string` | false    | The URL of the simulated ActivityPub account (https://KEYOXIDE_WEB_DOMAIN/user/keyoxide)
`ACTIVITYPUB_PRIVATE_KEY` | `string` | false    | The private key used to simulate an ActivityPub account and sign HTTP requests
`IRC_NICK`                | `string` | false    | The nick of your IRC verifier account
`MATRIX_ACCESS_TOKEN`     | `string` | false    | The access token of your Matrix verifier account
`MATRIX_INSTANCE`         | `string` | false    | The instance of your Matrix verifier account
`MATRIX_ROOM_ID`          | `string` | false    | Must be `!dBfQZxCoGVmSTujfiv:matrix.org`
`TELEGRAM_TOKEN`          | `string` | false    | The token for the Telegram Bot API
`XMPP_SERVICE`            | `string` | false    | The hostname of your XMPP verifier account
`XMPP_USERNAME`           | `string` | false    | The username of your XMPP verifier account
`XMPP_PASSWORD`           | `string` | false    | The password of your XMPP verifier account

<p class="info">All of these settings are optional but you will need to provide all the settings for each service (matrix, twitter, etc) for that verification to work.</p>

## Claim verification configuration

Some service providers require additional steps and server configuration to get identity verification working on their platforms.

If a service provider is not listed below, no further steps are required and identity verification is working right away.

### For ActivityPub claim verification

Some ActivityPub-compatible instances require their API calls to be signed. To accomplish this, the Keyoxide instance can simulate an ActivityPub account. The following commands generate the public and private keys needed for the server configuration:

```sh
openssl genrsa -out private.pem 2048
openssl rsa -in private.pem -outform PEM -pubout -out public.pem
while read -r line; do echo -nE "$line\n" ; done < public.pem > public-oneline.pem
```

### For IRC claim verification

Simply decide on a unique nickname that should always be available when a claim needs to be verified. There is no need to register the nickname beforehand on any IRC server.

### For Matrix claim verification

For Matrix verification to work, create a dedicated Matrix account and make it a member of the <a href="https://matrix.to/#/#doipver:matrix.org">#doipver:matrix.org room</a>. Provide the login details to the keyoxide-proxy instance.

### For Telegram claim verification

Follow the [standard Telegram instructions](https://core.telegram.org/bots/tutorial) to obtain an API token. In short, start a conversation with the [@BotFather](https://t.me/botfather), issue the `/newbot` command and follow the steps until you are given the API token.

### For XMPP claim verification

Create a dedicated XMPP account and provide the login details to the keyoxide-proxy instance.