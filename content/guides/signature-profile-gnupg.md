+++
title = "Legacy signature profiles - Using GnuPG"
aliases = ["/signature-profiles", "/signature-profiles/overview", "/signature-profiles/using-gnupg", "/getting-started/signature-profiles"]

weight = 3

[extra]
toc = true
+++

<p class="warning">Legacy signature profiles are currently deprecated in favor of <a href="/getting-started/creating-profile/">ASP profiles</a> and may stop working in the future.</p>

## Creating a profile

Using graphical tools like notepad, notepad++ or terminal tools like vim, emacs, nano, create a new plaintext document.

<p class="info">Plaintext documents usually have the <strong>.txt</strong> file extension. Do not use tools like Word for this purpose, it will not work!</p>

Add some text to make it obvious why you are making this particular signature profile or who its intended audience is:

```
Hey there! Here's a signature profile with proofs related to the DOIP project (https://doip.rocks).
```

Optionally, add some instructions for people who don't know what to do with a Keyoxide profile when they receive one:

```
Verify this profile at https://keyoxide.org/sig
```

<p class="info">If you use a different Keyoxide instance, feel free to link to that one instead!</p>

On this website, from the menu on the left, choose a service provider for which to verify an account. Follow the instructions on that page.

Add a new line for each identity claim (make sure to replace CLAIM with the information from the documentation page):

```
proof=CLAIM
```

<p class="info">We use the term <strong>proof=</strong> to signify that at the location of the claim, there is a proof that will validate the identity claim.</p>

The content could eventually look like this:

```
Hey there! Here's a signature profile with proofs related to the DOIP project (https://doip.rocks).

Verify this profile at https://keyoxide.org/sig

proof=dns:doip.rocks
proof=https://fosstodon.org/@keyoxide
```

### Generating an OpenPGP keypair

If you already have your own OpenPGP keypair, you can continue to [Signing the profile](#Signing_the_profile).

If not, generate your own OpenPGP key pair by following the instructions in the [OpenPGP with GnuPG guide](/using-cryptography/openpgp-gnupg) or the [OpenPGP with Kleopatra guide](/using-cryptography/openpgp-kleopatra).

### Signing the profile

You will now sign this plaintext document, making it untemperable and possible to prove beyond doubt that you, as holder of the private key, and only you could have signed the document and created this profile.

Sign the document (make sure to replace EMAIL_ADDRESS and FILENAME):

```bash
gpg -u EMAIL_ADDRESS --clear-sign FILENAME
```

This will generate a file named **FILENAME.asc** with the signed document. For example:

```
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Hey there! Here's a signature profile with proofs related to the DOIP project (https://doip.rocks).

Verify this profile at https://keyoxide.org/sig

proof=dns:doip.rocks
proof=https://fosstodon.org/@keyoxide
-----BEGIN PGP SIGNATURE-----

iQHEBAEBCgAuFiEENjcgJSPnwTCat56Z7y3FgntEX0sFAl/7L0MQHHRlc3RAZG9p
cC5yb2NrcwAKCRDvLcWCe0RfS3iYC/0QQqz2lzSNrkApdIN9OJFfd/sP2qeGr/uH
98YHa+ucwBxer6yrAaTYYuBJg1uyzdxQhqF2jWno7FwN4crnj15AN5XGemjpmqat
py9wG6vCVjC81q/BWMIMZ7RJ/m8F8Kz556xHiU8KbqLNDqFVcT35/PhJsw71XVCI
N3HgrgD7CY/vIsZ3WIH7mne3q9O7X4TJQtFoZZ/l9lKj7qk3LrSFnL6q+JxUr2Im
xfYZKaSz6lmLf+vfPc59JuQtV1z0HSNDQkpKEjmLeIlc+ZNAdSQRjkfi+UDK7eKV
KGOlkcslroJO6rT3ruqx9L3hHtrM8dKQFgtRSaofB51HCyhNzmipbBHnLnKQrcf6
o8nn9OkP7F9NfbBE6xYIUCkgnv1lQbzeXsLLVuEKMW8bvZOmI7jTcthqnwzEIHj/
G4p+zPGgO+6Pzuhn47fxH+QZ0KPA8o2vx0DvOkZT6HEqG+EqpIoC/a7wD68n789c
K2NLCVb9oIGarPfhIdPV3QbrA5eXRRQ=
=QyNy
-----END PGP SIGNATURE-----
```

This document is a fully functional signature profile! Try and verify the profile on [keyoxide.org/sig](https://keyoxide.org/sig).

## Distributing the profile

To distribute the profile, have a look at the [Distributing a signature profile](/signature-profiles/distributing) guide.

## Adding an identity claim

Open the plaintext document made with the [Creating a profile](#Creating_a_profile) guide and add a new identity claim on a new line. Make sure to sign the document again!

## Deleting a claim

Open the plaintext document made with the [Creating a profile](#Creating_a_profile) guide and remove the line containing the identity claim. Make sure to sign the document again!