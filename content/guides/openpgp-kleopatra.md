+++
title = "Using OpenPGP with Kleopatra"
aliases = ["/using-cryptography/openpgp-kleopatra"]

weight = 2

[extra]
toc = true
+++

## Installation

Install **Kleopatra**. It is available from most Linux package managers. For Windows users, Kleopatra is included in the [GPG4win](https://www.gpg4win.org/) suite of tools. Not available on MacOS.

## Generate a keypair

Generate a new cryptographic key by clicking on **File > New Key Pair**.

Choose **Create a personal OpenPGP key pair**.

Follow the instructions. It is recommended to protect the key with a strong passphrase.

Your fingerprint will appear when the key pair has been successfully created. You can always find it again by going to the **Certificates** tab and double clicking on your key. 

<p class="warning">A Key-ID is not the same thing as a fingerprint! Keyoxide guides will usually require you to ented the fingerprint, not the Key-ID.</p>

### Distributing via keys.openpgp.org

Your new OpenPGP keypair is stored locally on your computer — both the private key and the public key. To make your public key available to others, you need to upload it to a so-called keyserver. Keyoxide recommends [keys.openpgp.org](https://keys.openpgp.org) for this purpose.

Go to **Settings > Configure Kleopatra** and change the OpenPGP keyserver to:

```
hkps://keys.openpgp.org
```

Go to the **Certificates** tab, right click on your key and choose **Publish on Server**. Follow the instructions.

### Distributing via a WKD server

Please refer to the [Web Key Directory documentation](/web-key-directory).

You will need to export your public key. Go to the **Certificates** tab, right click on your key and choose **Export**. Follow the instructions.

### Changing the key expiry date

Go to the **Certificates** tab, right click on your key and choose **Change Expiry Date**. Follow the instructions.

## Obtaining the fingerprint

Go to the **Certificates** tab, locate and double-click on the correct key.

There is the fingerprint you are looking for.

## Signing a document

Press the big **Sign/Encrypt** button in the top bar. Follow the instructions.

## Verifying a signature

Press the big **Sign/Encrypt** button in the top bar. Follow the instructions.

## Working with notations

Kleopatra does not support working with notations.