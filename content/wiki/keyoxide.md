+++
title = "Keyoxide"
aliases = ["/understanding-keyoxide/keyoxide"]

[extra]
toc = true
+++

There is a lot to Keyoxide and decentralized identity verification, so let's divide the knowledge in three sections of increasing complexity.

## Basic

Keyoxide allows you to prove "ownership" or rather "hold" of accounts on websites, domain names, instant messaging, etc., regardless of your _username_.

That last part is important: you could, for example, have **alice** as username on one platform and **@alice24** on another. And if your personal website is **thatperson.tld**, how are people supposed to know that all that online property is yours? And if they are contacted by an **@alice42** account pretending to be you, how will they be able to spot the impostor?

Of course, one could opt for full anonymity! In which case, keep these properties as separated as possible.

But if you'd like to prove to others these accounts are yours and, by doing so, establish an online identity, you'll need a clever solution.

Enter Keyoxide.

When you visit someone's Keyoxide profile and see a green tick next to an account on some website, it was proven beyond doubt that the same person who set up this profile also holds that account.

Here's an [example account](https://keyoxide.org/hkp/test%40doip.rocks) to better understand what Keyoxide is about. The person that created this Keyoxide account claims to hold the **doip.rocks** domain. This is confirmed by the Keyoxide website which shows a green tick next to the tick. If you want to manually verify the claim, go to [dns-lookup.com/doip.rocks](https://dns-lookup.com/doip.rocks) and make sure that the fingerprints match under the TXT entries (there may be multiple TXT entries!).

## Intermediate

Keyoxide's purpose is just that: linking online properties together. Now, any service could easily claim to accomplish such a feat. To ensure it happens in a trustworthy manner, Keyoxide uses an _open source, decentralized and cryptography-based approach to bidirectional linking_.

Let's break down that sentence.

### Open source

Open source means: everyone can inspect the code behind Keyoxide. Really! Here, have a look at the code behind the main Keyoxide website: [https://codeberg.org/keyoxide/keyoxide-web](https://codeberg.org/keyoxide/keyoxide-web).

Not only can you look at it, you are allowed to make changes and even "fork it": take all this code and build your own product with it. _Allowed_? You are invited to! This keeps the Keyoxide project honest and always moving towards something that serves a greater good.

The Keyoxide project is licensed under [AGPL-3.0-or-later](https://codeberg.org/keyoxide/keyoxide-web/src/branch/main/LICENSE).

### Decentralized

The topic of decentralization is vast and complex. In short, it refers to the practice of keeping data in separate but connected places, instead of putting all the data in one single place.

Have you noticed how Google, Facebook and banks are desired targets for hackers? That is because they all use a centralized model. Break in once, get all the data.

Keyoxide uses decentralization on two levels: the profile data, and the identity verification process.

### Decentralized profile data

Where does Keyoxide get the data from to display all these profile pages?

To make a Facebook profile, you need to give your data to them and let them store it on their servers, that's how they can display Facebook profiles.

Rest assured, Keyoxide does not want your data on its servers. Nor does it need it.

You put your data in either so-called [cryptographic signatures](/signature-profiles) or [OpenPGP keys](/openpgp/profiles) which will act as _digital passports_. You could try and conceptualize these digital passports as _transparent vaults_. Everyone (including Keyoxide) can look inside the transparent vault and see your data, but no one except you can change it or delete it. You have full control over your data. You can store it where you want: on a so-called key server, on your own server. You can even just send it by email or a messaging platform.

So what could a digital passport look like?

```
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Hey there! Here's a signature profile with proofs related to the DOIP project (https://doip.rocks).

Verify this profile at https://keyoxide.org/sig

proof=dns:doip.rocks
proof=https://fosstodon.org/@keyoxide
-----BEGIN PGP SIGNATURE-----

iQHEBAEBCgAuFiEENjcgJSPnwTCat56Z7y3FgntEX0sFAl/7L0MQHHRlc3RAZG9p
cC5yb2NrcwAKCRDvLcWCe0RfS3iYC/0QQqz2lzSNrkApdIN9OJFfd/sP2qeGr/uH
98YHa+ucwBxer6yrAaTYYuBJg1uyzdxQhqF2jWno7FwN4crnj15AN5XGemjpmqat
py9wG6vCVjC81q/BWMIMZ7RJ/m8F8Kz556xHiU8KbqLNDqFVcT35/PhJsw71XVCI
N3HgrgD7CY/vIsZ3WIH7mne3q9O7X4TJQtFoZZ/l9lKj7qk3LrSFnL6q+JxUr2Im
xfYZKaSz6lmLf+vfPc59JuQtV1z0HSNDQkpKEjmLeIlc+ZNAdSQRjkfi+UDK7eKV
KGOlkcslroJO6rT3ruqx9L3hHtrM8dKQFgtRSaofB51HCyhNzmipbBHnLnKQrcf6
o8nn9OkP7F9NfbBE6xYIUCkgnv1lQbzeXsLLVuEKMW8bvZOmI7jTcthqnwzEIHj/
G4p+zPGgO+6Pzuhn47fxH+QZ0KPA8o2vx0DvOkZT6HEqG+EqpIoC/a7wD68n789c
K2NLCVb9oIGarPfhIdPV3QbrA5eXRRQ=
=QyNy
-----END PGP SIGNATURE-----
```

This text above is all the data Keyoxide needs to generate a profile page. Really! Copy and paste it in [keyoxide.org/sig](https://keyoxide.org/sig) and watch Keyoxide verify this profile.

Everyone can read the top section. But no one can modify it because all the characters in the bottom section guarantee the validity of the top section. Change a single character in the text above and the signature gets invalidated.

Try it out and see for yourself! Change any character in either the profile text or in the signature at the bottom and let [keyoxide.org/sig](https://keyoxide.org/sig) do its thing. It will let you know something is not correct!

A true _transparent vault_.

### Decentralized identity verification

The process itself of verifying identity claims (more on this in the [bidirectional linking](#Bidirectional_linking) section) is also decentralized. You do not need to contact Keyoxide servers for most verifications.

A Hackernews account can be verified by looking at the content of its _about_ section. Most Fediverse accounts can be verified by looking at their _biography_ section.

Keyoxide does the same thing. If you view a profile page on any of the [clients](/clients), it will automate the identity verification process by directly contacting Hackernews and the Fediverse. No intermediary servers required.

The less intermediary servers are required, the more trustworthy the process becomes.

<p class="info">Some accounts can't be verified directly by the Keyoxide web client. For these cases, Keyoxide relies on a <a href="#Proxy">proxy server</a>. Other clients like mobile apps usually don't have such limitations.</p>

### Cryptography

Identity claims are created and managed inside cryptographic signatures or keys that act as _secure transport vessels_.

What makes cryptographic keys so useful for [OpenPGP profiles](/openpgp-profiles) is that they are actually made of two keys: a _private key_ and a _public key_. Together, they form a _key pair_.

Everyone has access to the public key. This is the _transparent vault_ itself: everyone can see it, but no one can modify its content. You can safely share your public key.

The transparent vault can only be opened and modified using the private key, which is usually nothing more than a file with seemingly random characters. The private key is yours and yours alone and should never be shared.

It goes without saying that losing the private key means losing access to the public key. Likewise, someone who steals your private key can easily modify the contents of your public key. Handling cryptographic keys is no simple task, a process where security always takes precedence over convenience.

Keyoxide uses the widely-used and well-known [OpenPGP standard](https://www.openpgp.org).

When it comes to [signature profiles](/signature-profiles), the reasoning is almost the same. In this case, the _transparent vault_ is a piece of text secured by a cryptographic signature, making it so they can't be modified unless one has the private key. The public key can be used by anyone to ensure that the signature's origin is genuine.

The private key creates the vault, the public key makes it transparent.

### Bidirectional linking

How does one establish an _anonymous identity_ — a _persona_ — without revealing their personal identity?

Let's consider this slightly unusual scenario: how could I prove I own a car that is sitting idle by the side of the road and wasn't just abandoned there by someone else?

We do not want to use any "official" services (which are centralized) so checking license plates is out of the question. I would also like to remain anonymous in the process so identity cards are also not the solution.

Obviously, I can't link the car to my person without revealing my personal identity.

But what if I could link it to my house? That way, _whoever owns this house also owns this car_. By doing so, I would establish an anonymous identity (_someone_ own this house and that same _someone_ own this car) without ever revealing my personal identity.

All I need to do is place a note underneath the windscreen with my home address on it. This will be a transparent vault! Everyone can read it since the windshield is just glass, but only the person with the car keys can modify the note.

I have now claimed my car in a fully decentralized manner without the need to involve any centralized organization. Is this sufficient? Yes and no.

Only I could have placed that note there so that proves beyond doubt my access to the car.

But what if someone leaves their car in my street with a note containing my home address? This attempt at impersonation lets them claim that I own that car because it is now linked to my house!

This is why we need **bidirectional linking**: not only does my car need a note underneath the windshield with my home address on it, my home needs a note behind a window with the car's license plate on it. As my home now clearly states my real car's license plate, no one can pretend I abandoned their car by the side of the road.

An unusual scenario indeed, but one that simplifies the stakes yet illustrates the process. Keyoxide allows you to establish an online identity while remaining anonymous: no one needs to know who you are, but you can still prove you hold accounts on different websites.

Your Keyoxide profile points to all your accounts and all your accounts point back to your Keyoxide profile.

## Advanced

By now, you should have all the knowledge to understand what is going on and get started. Here are a few more advanced topics.

### Proxy

This section involves almost exclusively [Keyoxide's web client](https://keyoxide.org). Other [clients](/clients) like the mobile app do not need a proxy under normal circumstances.

Some services DNS records require a complicated verification processes that cannot be performed by a browser. In such cases, the browser will ask a Keyoxide proxy server to do the verification instead.

Since this is the internet we are talking about, you should always be skeptical about data that comes from unknown servers. In order to mitigate this, each Keyoxide profile page will let you know when a proxy was used and invite you to perform the identity verification again using a client that does not need a proxy server.

### Keyoxide instances

The Keyoxide website was built with the idea that other people could put it on their servers as well. We call these _instances_. The Keyoxide project's maintainer has put an instance on [https://keyoxide.org](https://keyoxide.org) but that is not the only way to access Keyoxide. Everyone could put it on their servers.

The idea is simple: you will most likely not know the maintainer, so why should you trust his website [https://keyoxide.org](https://keyoxide.org)? By making the Keyoxide website selfhostable, you could put it yourself on your own server, or ask a friend to put it on theirs.

More information in the [self hosting guide](/advanced/self-hosting).

Of course, use any of the other non-web [clients](/clients) to avoid many of these trust issues, as these clients will perform the identity verification locally on the device.

### Funding

With surveillance capitalism on the rise, it's important to know where money comes from to understand whose interests are served, especially with a project that involves online identity and anonymity!

The project is fully funded by donations. There are no fees to using or hosting Keyoxide. There are no ads. There is no tracking. There are no investors.

All donations come from the people and the organizations that see the need for a project like Keyoxide to exist, be universally accessible and remain free of external interests.

Donations go to the [Key To Identity Foundation](https://keytoidentity.foundation/), founded by the maintainer for the purpose of promoting and sustaining the Keyoxide projects and other future online identity projects. 

If you'd like to donate as well, please have a look at the [foundation's donate page](https://keytoidentity.foundation/donate). All contributions are much appreciated and help the maintainer to fully commit to the Keyoxide project.
