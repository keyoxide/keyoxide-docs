+++
title = "Cryptographic tools"
aliases = ["/cryptography", "/cryptography-programs", "/openpgp"]
+++

## OpenPGP

Here's the [Wikipedia page on PGP](https://en.wikipedia.org/wiki/Pretty_Good_Privacy) which has a chapter dedicated to [OpenPGP](https://en.wikipedia.org/wiki/Pretty_Good_Privacy#OpenPGP).

In short, OpenPGP is a popular and widely used asymmetric cryptography standard described in the IETF's [RFC 4880](https://www.rfc-editor.org/rfc/rfc4880.html). OpenPGP key pairs can contain UserIDs and subkeys which can encrypt/decrypt or sign/verify data.

Keyoxide uses OpenPGP for both types of profiles: [signature profiles](/signature-profiles) and [openpgp profiles](/openpgp-profiles).