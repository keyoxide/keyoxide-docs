+++
title = "Comparing the profile types"
aliases = ["/getting-started/comparing-profile-types", "/understanding-keyoxide/comparing-profile-types"]
+++

## Regarding profile management

Feature | Signature profiles | OpenPGP profiles
:-------|:-------------------|:----------------
Identity claims | Stored as text | Stored as OpenPGP notations
Graphical tools | Yes → [Kleopatra](/signature-profiles/using-kleopatra) | No
Command line tools | Yes → [GnuPG](/signature-profiles/using-gnupg) | Yes → [GnuPG](/openpgp-profiles/using-gnupg)
Granularity | Many different profiles per key pair | Only one profile per key pair

## Regarding profile distribution

Feature | Signature profiles | OpenPGP profiles
:-------|:-------------------|:----------------
Direct method | Yes → send text | Yes → send public key file
Via personal website | Yes → embed text | Yes → embed public key file
Via personal server | No | Yes → upload using [WKD](/web-key-directory)
Via centralized servers | No | Yes → upload to keyservers
Distributed together with public key | No (not yet) | Yes → public key contains profile

## Regarding anonymity

Feature | Signature profiles | OpenPGP profiles
:-------|:-------------------|:----------------
Real name | Not required | Not required
Valid email address | Not required | Required when using keyservers

## Regarding cryptography

Feature | Signature profiles | OpenPGP profiles
:-------|:-------------------|:----------------
Security | Secured by cryptography | Secured by cryptography
Choice of standard | Only OpenPGP (for now) | Only OpenPGP

## The advantages and drawbacks of signature profiles

Storing identity claims inside the public key as notations for OpenPGP profiles is a powerful method. Wherever the public key goes, so go the identity claims. This allows one to use the existing vast network of OpenPGP key sharing tools to also share these identity claims.

There are drawbacks to this: you lose granularity. You cannot pick and choose the identity claims you want to send to certain people or use for certain purposes. There is also the possibility that notations in public keys could be scraped as the keys are publicly available.

Putting (only certain) identity claims in a signature profile solves both drawbacks. You can choose which identity claims are to be associated with each other and you can choose which persons can see this by only sending it to them. You can even encrypt the signature profile! Since the signature profile is not publicly available (unless you make it so), there is no possibility to scrape the contents of it.

Note that there is one catch: the person you send your signature profile to could publish it. Only send identity claims you intend to keep secret to people you trust!