+++
title = "Identity claims and proofs"
aliases = ["/advanced/openpgp-proofs", "/getting-started/identity-claims"]
+++

## What are identity claims and proofs?

Identity claims allow you to _claim_ something is part of your identity, and identity proofs _prove_ the validity of the claims.

Let's use some examples to understand this concept.

To understand identity claims and proofs using a simple analogy, please read the [Bidirectional linking](/wiki/keyoxide/#Bidirectional_linking) section of the documentation.

Consider the following example:

* Alice and Bob have been talking for years on service A. Alice already has an account on service B.

Scenario 1:

* Bob wants to move to service B as well.
* Alice makes two identity claims, one for her account on service A and one for her account on service B. Alice takes the appropriate steps to provide the proofs for both claims.
* Bob obtains Alice's Keyoxide profile and sees both identity claims marked as verified. This confirms that the person Bob knows as Alice on service A is also known as Alice on service B. Bob can safely move to service B and talk to Alice without having to meet in person to confirm their accounts.

Scenario 2:

* Alice receives a friend request from Bob29 on service C. Is this the same Bob from service A or not?
* Alice obtains Bob29's Keyoxide profile. It shows a verified claim for Bob on platform A and a verified claim for Bob29 on service C. Yes, it is the same Bob — there were just already 28 Bobs using service C, so Bob's preferred username was already taken.

Scenario 3:

* Bob receives a friend request from Alyce on service D. Is this the same Alice from service A or not?
* Bob obtains Alyce's Keyoxide profile. It shows a verified claim for Alyce on service D but not one for Alice from service A. It is most certainly not Alice from service A and possibly even an attempt at impersonation.

Using this concept of identity claims and proofs, people can establish online identities, link accounts together to prove their legitimacy and detect attemps at impersonation.

## Keyoxide claims and proofs

For Keyoxide, a claim is simply a link to an account on some website, or something similar. See any of the service providers listed in this documentation for examples of what claims may look like.

For Keyoxide, a proof is simply a bit of text mentioning the so-called fingerprint of people's Keyoxide profile (or rather, the cryptographic key used to create it) that they can put in their account's "Biography", post on a social network, or something similar. Here is [what identity proofs look like](/wiki/identity-proof-formats/).

Both Keyoxide claims and proofs are meant to be publicly visible so that anyone can verify them for themselves. 