+++
title = "Cryptographic fingerprint"
aliases = ["/fingerprint", "/how-to-obtain-fingerprint"]
+++

## For signature profiles using an OpenPGP key

Obtain the fingerprint using [GnuPG](/using-cryptography/openpgp-gnupg/#Obtaining_the_fingerprint) or [Kleopatra](/using-cryptography/openpgp-kleopatra/#Obtaining_the_fingerprint).

If the guide asks for FINGERPRINT, use the fingerprint directly.  
If the guide asks for FINGERPRINT_URI, use:

```
openpgp4fpr:FINGERPRINT
```

## For OpenPGP profiles

Obtain the fingerprint using [GnuPG](/using-cryptography/openpgp-gnupg/#Obtaining_the_fingerprint) or [Kleopatra](/using-cryptography/openpgp-kleopatra/#Obtaining_the_fingerprint).

If the guide asks for FINGERPRINT, use the fingerprint directly.  
If the guide asks for FINGERPRINT_URI, use:

```
openpgp4fpr:FINGERPRINT
```