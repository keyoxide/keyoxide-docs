+++
title = "FAQ"
aliases = ["/faq"]

[extra]
toc = true
+++

## What is Keyoxide? The Ariadne Spec? DOIP?

Keyoxide is a privacy-friendly tool to maintain decentralized anonymous online identities. Like a digital passport but anonymous if you want to.

Here's a [detailed explanation](/wiki/keyoxide) of how it works.

[doip.js](https://doip.rocks/) is the JavaScript library developed by the Keyoxide project to power the different Keyoxide clients like [keyoxide.org](https://keyoxide.org). If you are a developer, you can use the same tools as Keyoxide and build your own clients and share them with the world.

The [Ariadne Spec](https://ariadne.id) is the specification that describes the whole decentralized online identity verification process. This is what you will need to build your own libraries so that all tools are compatible with each other.

## Is Keyoxide open source?

Yes, the Keyoxide project is fully open source, with the repositories licensed to either Apache or AGPL. Our git forge of choice is [Codeberg.org](https://codeberg.org).

[{{ get_it_on_codeberg() }}](https://codeberg.org/keyoxide)

## How is this project funded?

The Keyoxide project is entirely funded by donations from individuals and groups, notably through [Liberapay](https://liberapay.com/keyoxide).

Much of the work has been funded by the [NLnet foundation](https://nlnet.nl/) and [NGI0](https://www.ngi.eu/) for which the project is very grateful.

## My Keyoxide profile isn't working!

For it to work right, Keyoxide requires a lot of interaction with other platforms and services. Unfortunately, things do break sometimes. You might find the answer in the [Something went wrong](/getting-started/something-went-wrong) section. You can also try and see if the issue has been mentioned on the [Community Forum](https://community.keyoxide.org) and if not, start a new discussion.

## Why should I trust Keyoxide profiles?

Keyoxide simply faciltates and automates a process you can do manually. On every Keyoxide profile, you will find a link to the proof which you can try and visit by yourself. If you find the fingerprint mentioned somewhere, then it links back to the Keyoxide profile — and this is really most of what Keyoxide does for you.

## Can I prove my online identity without Keyoxide?

You most certainly can!

Let's say you want to prove your Twitter account and your Lichess account are both yours. Simply write in your Twitter bio the name of your Lichess account, and add your Twitter handle to your Lichess' bio. Done! Online identity established and it even is verifiable!

Now you want to add your Lobsters account to your identity. You'll need to update your Twitter bio, your Lichess bio and link to both from your Lobsters account.

Now you want to add your Fediverse account. You'll need to update your Twitter Bio, your Lichess bio, your Lobsters bio and link to all from your Fediverse account.

Now you want to add your Matrix account. And your website. And your IRC handle…

The more accounts you add, the more complex it will be to maintain your online identity.

Keyoxide aims to be your "digital passport": all accounts link to your passport, and your passport links to all your accounts.

## Can Keyoxide add account verification for website X?

Maybe, it depends on the website. You are invited to see if someone else proposed it in the [Service providers section of the forum](https://community.keyoxide.org/t/service-providers). If it hasn't yet, feel free to do so!

## Decentralized? So where is the blockchain?

_Decentralized_ is not the same thing as the _blockchain_. You don't need the blockchain to build decentralized tools. Keyoxide does not work with blockchains nor will it ever.

## What data does Keyoxide store?

On [keyoxide.org](https://keyoxide.org), no data is stored from the user's visit, not even server logs. There are no trackers, no cookies and no statistics.

The Keyoxide project cannot make the same claims for other Keyoxide instances, as it is possible for their respective administrators to have different server settings and potentially alter the source code.

[keyoxide.org](https://keyoxide.org) does not host profiles. The cryptographic keys — and the profiles they represent — must be hosted on keyservers or people's homeservers.